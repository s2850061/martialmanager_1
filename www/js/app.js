// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var martialManager = angular.module('starter', ['ionic', 'firebase'])
var fb = null;

martialManager.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
    fb = new Firebase("https://luminous-inferno-1749.firebaseio.com/");
  });
});

martialManager.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'LoginController'
    })
    .state('menu', {
      url: '/menu',
      templateUrl: 'templates/menu.html',
      controller: 'MenuController'
    })
    .state('student', {
      url: '/student',
      templateUrl: 'templates/student.html',
      controller: 'StudentController'
    })
    $urlRouterProvider.otherwise('/login');
});

martialManager.controller("LoginController", function($scope, $firebaseAuth, $location) {
 
    $scope.login = function(username, password) {
        var fbAuth = $firebaseAuth(fb);
        fbAuth.$authWithPassword({
            email: username,
            password: password
        }).then(function(authData) {
            $location.path("/menu");
        }).catch(function(error) {
            alert(error);
        });
    }
    
    $scope.logout = function() {
      var fbAuth = $firebaseAuth(fb);
        fb.unauth();
        alert("You are now logged out");
        $location.path("/login");
    };
 
    $scope.register = function(username, password) {
        var fbAuth = $firebaseAuth(fb);
        fbAuth.$createUser({email: username, password: password}).then(function() {
            return fbAuth.$authWithPassword({
                email: username,
                password: password
            });
        }).then(function(authData) {
            $location.path("/menu");
            alert("Thank you for registering");
        }).catch(function(error) {
            alert(error);
        });
    }
});

martialManager.controller("StudentController", function($scope, $firebaseObject, $ionicPopup, $ionicHistory) {
    
    $scope.list = function() {
      fbAuth = fb.getAuth();
      if(fbAuth) {
          var syncObject = $firebaseObject(fb.child("users/" + fbAuth.uid));
          syncObject.$bindTo($scope, "data");
      }
    }
 
    $scope.create = function() {
      $ionicPopup.prompt({
       title: 'Enter Student Details',
        inputType: 'text',
      })
      .then(function(result) {
        if(result !== "") {
            if($scope.data.hasOwnProperty("students") !== true) {
                $scope.data.students = [];
            }
            $scope.data.students.push({title: result});
        }
        else {
          alert("Action not completed");
        }
      });
    }
});

martialManager.controller("MenuController", function($scope) {
});


